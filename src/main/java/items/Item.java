package items;

import com.googlecode.lanterna.TextColor;

public class Item {
    public enum eqipSlot {HEAD, CHEST, BOOTS, ONE_HAND, TWO_HANDS, GLOVES, BACK, CONSUMEABLE}
    private String name;
    private char displayChar = '*';
    private int weight;
    private int durability;
    private int maxdurability;
    private int x;
    private int y;
    private eqipSlot slot;
    private TextColor textColor;

    public void onMove () {

    }
    public void onEquip() {

    }
    public void onAttack() {

    }

    public eqipSlot getSlot() {
        return slot;
    }

    public void setSlot(eqipSlot slot) {
        this.slot = slot;
    }



    public TextColor getTextColor() {
        return textColor;
    }

    public void setTextColor(TextColor textColor) {
        this.textColor = textColor;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    private void onDrop() {}
    private void onPickUp() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getDisplayChar() {
        return displayChar;
    }

    public void setDisplayChar(char displayChar) {
        this.displayChar = displayChar;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getDurability() {
        return durability;
    }

    public void setDurability(int durability) {
        this.durability = durability;
    }

    public int getMaxdurability() {
        return maxdurability;
    }

    public void setMaxdurability(int maxdurability) {
        this.maxdurability = maxdurability;
    }


}
