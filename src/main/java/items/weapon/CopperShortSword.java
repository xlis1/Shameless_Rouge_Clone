package items.weapon;

import com.googlecode.lanterna.TextColor;

public class CopperShortSword extends Weapon {
    public CopperShortSword(int x, int y) {
        this.setX(x);
        this.setY(y);
        this.setAccuracy(60);
        this.setAttackBonus(1);
        this.setDamageType(damageTypes.SLASH);
        this.setMaxHit(15);
        this.setName("Copper Short Sword");
        this.setTextColor(TextColor.ANSI.YELLOW);
        this.setSlot(eqipSlot.ONE_HAND);
    }
}
