package items.weapon;

import items.Item;

public class Weapon extends Item {
    /*
    just some brainstorming

    offhand works as a modfyer to main had weapon?
    sword + focus = magic sword slash
    (damage bonus from focus tied to effecteiveness of main hand weapon against opponent armor)

     */
    private int accuracy;
    private int maxHit;
    private int attackBonus;
    public enum damageTypes {SLASH, STAB, BLUNT, MAGIC}
    private damageTypes damageType;


    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public int getMaxHit() {
        return maxHit;
    }

    public void setMaxHit(int maxHit) {
        this.maxHit = maxHit;
    }

    public int getAttackBonus() {
        return attackBonus;
    }

    public void setAttackBonus(int attackBonus) {
        this.attackBonus = attackBonus;
    }

    public damageTypes getDamageType() {
        return damageType;
    }

    public void setDamageType(damageTypes damageType) {
        this.damageType = damageType;
    }
}
