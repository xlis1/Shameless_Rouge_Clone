package items;

import java.util.LinkedList;

public class InventoryUpdate {
    /*
    oh god this is one hell of a hack but i guess it will have to do for now
     */


    private Item[] inventory;
    private LinkedList<Item> groundItems;
    private Item[] equippedItems;


    public InventoryUpdate(Item[] inventory, Item[] equippedItems, LinkedList<Item> groundItems) {
        this.inventory = inventory;
        this.groundItems = groundItems;
        this.equippedItems = equippedItems;
    }

    public Item[] getInventory() {
        return inventory;
    }

    public void setInventory(Item[] inventory) {
        this.inventory = inventory;
    }

    public LinkedList<Item> getGroundItems() {
        return groundItems;
    }

    public void setGroundItems(LinkedList<Item> groundItems) {
        this.groundItems = groundItems;
    }

    public Item[] getEquippedItems() {
        return equippedItems;
    }

    public void setEquippedItems(Item[] equippedItems) {
        this.equippedItems = equippedItems;
    }
}
