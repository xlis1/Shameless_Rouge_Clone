package screens;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.terminal.Terminal;
import entities.Player;
import items.InventoryUpdate;
import items.Item;

import java.io.IOException;
import java.util.LinkedList;

public class Inventory {
    /*
    so im gonna mock up what i think i want the inventory to look like here

    Inventory           | equiped           |info
    ==========================================================
    list of items in inv|list of equiped items| info for currently highlighted weapon
     */

    public InventoryUpdate displayInventory(Item[] inventory, Item[] equipedItems, LinkedList<Item> groundItems, Terminal terminal, TextGraphics textGraphics, Player player) throws IOException {
        /*
        draw boxes
        draw item names
        draw ui labels
        draw highlight
        flush
        take input
        do stuff with input
         */
        int currentX = 0;
        int currentY = 0;
        Item temp;

        //main input/draw loop
        while (true) {
            terminal.clearScreen();
        TerminalPosition topLeft = new TerminalPosition(0,0);
        TerminalPosition topRight = new TerminalPosition(terminal.getTerminalSize().getColumns()- 1, 0 );
        TerminalPosition bottomLeft = new TerminalPosition(0, terminal.getTerminalSize().getRows()-1);
        TerminalPosition bottomRight = new TerminalPosition(terminal.getTerminalSize().getColumns()-1, terminal.getTerminalSize().getRows()- 1);
        TerminalPosition firstThirdDivideTop = new TerminalPosition(terminal.getTerminalSize().getColumns()/3, 0);
        TerminalPosition firstThirdDivideBottom = new TerminalPosition(terminal.getTerminalSize().getColumns()/3, terminal.getTerminalSize().getRows()-1);
        TerminalPosition secondThirdDivideTop = new TerminalPosition((terminal.getTerminalSize().getColumns()/3) * 2, 0);
        TerminalPosition secondThirdDivideBottom = new TerminalPosition((terminal.getTerminalSize().getColumns()/3) * 2, terminal.getTerminalSize().getRows()-1);
        terminal.clearScreen();
        //start of basic outline (WORKING)
        textGraphics.drawLine(topLeft, topRight, '═');
        textGraphics.drawLine(bottomLeft, bottomRight, '═');
        textGraphics.drawLine(topLeft, bottomLeft, '║');
        textGraphics.drawLine(topRight, bottomRight, '║');
        textGraphics.setCharacter(bottomLeft, '╚');
        textGraphics.setCharacter(bottomRight, '╝');
        textGraphics.setCharacter(topLeft, '╔');
        textGraphics.setCharacter(topRight, '╗');
        textGraphics.setCharacter(topLeft.getColumn(), topLeft.getRow() +2, '╠');
        textGraphics.setCharacter(topRight.getColumn(), topRight.getRow() +2, '╣');
        textGraphics.drawLine(topLeft.getColumn() + 1, topLeft.getRow() + 2, topRight.getColumn() - 1, topRight.getRow()+2, '═');
        //end of basic box outline
        //start of 3rd dividers
        //start of first third (WORKING)
        textGraphics.drawLine(firstThirdDivideTop.getColumn(), firstThirdDivideTop.getRow(), firstThirdDivideBottom.getColumn(), firstThirdDivideBottom.getRow(), '║');
        textGraphics.setCharacter(firstThirdDivideTop.getColumn(), firstThirdDivideTop.getRow(), '╦');
        textGraphics.setCharacter(firstThirdDivideTop.getColumn(), firstThirdDivideTop.getRow() + 2, '╬');
        textGraphics.setCharacter(firstThirdDivideBottom, '╩');
        //end of first third
        //start of second third (WORKING)
        textGraphics.drawLine(secondThirdDivideTop.getColumn(), secondThirdDivideTop.getRow(), secondThirdDivideBottom.getColumn(), secondThirdDivideBottom.getRow(), '║');
        textGraphics.setCharacter(secondThirdDivideTop.getColumn(), secondThirdDivideTop.getRow(), '╦');
        textGraphics.setCharacter(secondThirdDivideTop.getColumn(), secondThirdDivideTop.getRow() + 2, '╬');
        textGraphics.setCharacter(secondThirdDivideBottom, '╩');
        //end of second third
        //Start of static string placement
        textGraphics.putString(topLeft.getColumn() + 1, topLeft.getRow() + 1, "screens");
        textGraphics.putString(firstThirdDivideTop.getColumn() + 1, firstThirdDivideTop.getRow() + 1, "Equiped Items");
        textGraphics.putString(secondThirdDivideTop.getColumn() + 1, secondThirdDivideTop.getRow() +1, "Inspect Stats");
        //end of static strings
        //start of non static strings and user input
        //start of printing player inventory (WORKING)



            for (int i = 0; i < inventory.length; i++) {
                if (inventory[i] != null && inventory[i].getName().length() < firstThirdDivideTop.getColumn() - 3) {
                    if (currentX == 0 && currentY == i) {
                        textGraphics.setForegroundColor(TextColor.ANSI.BLACK);
                        textGraphics.setBackgroundColor(TextColor.ANSI.WHITE);
                    }
                    else {
                        textGraphics.setForegroundColor(inventory[i].getTextColor());
                    }
                    textGraphics.putString(3, 4 + i, inventory[i].getName());
                    textGraphics.setBackgroundColor(TextColor.ANSI.DEFAULT);
                    textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);
                }
                else if (inventory[i] != null) {
                    if (currentX == 0 && currentY == i) {
                        textGraphics.setForegroundColor(TextColor.ANSI.BLACK);
                        textGraphics.setBackgroundColor(TextColor.ANSI.WHITE);
                    }
                    else {
                        textGraphics.setForegroundColor(inventory[i].getTextColor());
                    }
                    textGraphics.setForegroundColor(inventory[i].getTextColor());
                    for (int j = 0; j < firstThirdDivideBottom.getColumn()-6; j++) {
                        textGraphics.setCharacter(3+j, i+3, inventory[i].getName().charAt(j));
                    }
                    textGraphics.putString(firstThirdDivideTop.getColumn()-4, i+3, "...");
                    textGraphics.setBackgroundColor(TextColor.ANSI.DEFAULT);
                    textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);
                }
                else {
                    if (currentX == 0 && currentY == i) {
                        textGraphics.setForegroundColor(TextColor.ANSI.BLACK);
                        textGraphics.setBackgroundColor(TextColor.ANSI.WHITE);
                    }
                    else {
                        textGraphics.setBackgroundColor(TextColor.ANSI.DEFAULT);
                        textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);
                    }
                    textGraphics.setCharacter(topLeft.getColumn() + 3, i+4, '┉');
                    textGraphics.setBackgroundColor(TextColor.ANSI.DEFAULT);
                    textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);
                }
            }
            //end of printing inventory
            //start of printing equiped items (WORKING)
            for (int i = 0; i < equipedItems.length; i++) {
                if (equipedItems[i] != null && equipedItems[i].getName().length() < firstThirdDivideTop.getColumn() - 3) {
                    if (currentX == 1 && currentY == i) {
                        textGraphics.setForegroundColor(TextColor.ANSI.BLACK);
                        textGraphics.setBackgroundColor(TextColor.ANSI.WHITE);
                    }
                    else {
                        textGraphics.setForegroundColor(equipedItems[i].getTextColor());
                    }
                    textGraphics.setForegroundColor(equipedItems[i].getTextColor());
                    textGraphics.putString(firstThirdDivideTop.getColumn() +3, 4 + i, equipedItems[i].getName());
                    textGraphics.setBackgroundColor(TextColor.ANSI.DEFAULT);
                    textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);
                }
                else if (equipedItems[i] != null) {
                    if (currentX == 1 && currentY == i) {
                        textGraphics.setForegroundColor(TextColor.ANSI.BLACK);
                        textGraphics.setBackgroundColor(TextColor.ANSI.WHITE);
                    }
                    else {
                        textGraphics.setForegroundColor(equipedItems[i].getTextColor());
                    }
                    textGraphics.setForegroundColor(equipedItems[i].getTextColor());
                    for (int j = 0; j < secondThirdDivideBottom.getColumn()-6; j++) {
                        textGraphics.setCharacter(firstThirdDivideTop.getColumn() +3+j, i+3, equipedItems[i].getName().charAt(j));
                    }
                    textGraphics.putString(firstThirdDivideTop.getColumn()-4, i+3, "...");
                    textGraphics.setBackgroundColor(TextColor.ANSI.DEFAULT);
                    textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);
                }
                else {
                    if (currentX == 1 && currentY == i) {
                        textGraphics.setForegroundColor(TextColor.ANSI.BLACK);
                        textGraphics.setBackgroundColor(TextColor.ANSI.WHITE);
                    }
                    else {
                        textGraphics.setBackgroundColor(TextColor.ANSI.DEFAULT);
                        textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);
                    }
                    textGraphics.setCharacter(firstThirdDivideTop.getColumn() +3, i+4, '┉');
                    textGraphics.setBackgroundColor(TextColor.ANSI.DEFAULT);
                    textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);
                }
            }
            //end of printing equipped items




            terminal.flush();
            KeyStroke input = terminal.readInput();
            if (input.getKeyType() == KeyType.Escape) {
                break;
            }
            if (input.getCharacter() != null) {
                switch (input.getCharacter()) {
                    case 'e':
                        System.out.println("Highlighted item:");
                        if (currentX == 0) {
                            System.out.println(inventory[currentY]);
                            // EQUIPPED ITEMS FORMAT: HEAD, CHEST, GLOVES, BOOTS, BACK, ONE_HAND, TWO_HANDS
                            switch (inventory[currentY].getSlot()) {
                                case HEAD:
                                    if (equipedItems[0] != null) {
                                        temp = equipedItems[0];
                                        equipedItems[0] = inventory[currentY];
                                        inventory[currentY] = temp;
                                    }
                                    else {
                                        equipedItems[0] = inventory[currentY];
                                        inventory[currentY] = null;
                                    }
                                    break;
                                case CHEST:
                                    if (equipedItems[1] != null) {
                                        temp = equipedItems[1];
                                        equipedItems[1] = inventory[currentY];
                                        inventory[currentY] = temp;
                                    }
                                    else {
                                        equipedItems[1] = inventory[currentY];
                                        inventory[currentY] = null;
                                    }
                                    break;
                                case GLOVES:
                                    if (equipedItems[2] != null) {
                                        temp = equipedItems[2];
                                        equipedItems[2] = inventory[currentY];
                                        inventory[currentY] = temp;
                                    }
                                    else {
                                        equipedItems[2] = inventory[currentY];
                                        inventory[currentY] = null;
                                    }
                                    break;
                                case BOOTS:
                                    if (equipedItems[3] != null) {
                                        temp = equipedItems[3];
                                        equipedItems[3] = inventory[currentY];
                                        inventory[currentY] = temp;
                                    }
                                    else {
                                        equipedItems[3] = inventory[currentY];
                                        inventory[currentY] = null;
                                    }
                                    break;
                                case BACK:
                                    if (equipedItems[4] != null) {
                                        temp = equipedItems[4];
                                        equipedItems[4] = inventory[currentY];
                                        inventory[currentY] = temp;
                                    }
                                    else {
                                        equipedItems[4] = inventory[currentY];
                                        inventory[currentY] = null;
                                    }
                                    break;
                                case ONE_HAND:
                                    if (equipedItems[5] != null) {
                                        temp = equipedItems[5];
                                        equipedItems[5] = inventory[currentY];
                                        inventory[currentY] = temp;
                                    }
                                    else {
                                        equipedItems[5] = inventory[currentY];
                                        inventory[currentY] = null;
                                    }
                                    break;
                                case TWO_HANDS:
                                    if (equipedItems[5] != null) {
                                        temp = equipedItems[5];
                                        equipedItems[5] = inventory[currentY];
                                        inventory[currentY] = temp;
                                        for (int i = 0; i < inventory.length; i++) {
                                            if (inventory[i]==null) {
                                                inventory[i] = equipedItems[6];
                                            }
                                        }
                                        if (equipedItems[6] != null) {
                                            equipedItems[6].setX(player.getX());
                                            equipedItems[6].setY(player.getY());
                                            groundItems.add(equipedItems[6]);
                                            equipedItems[6] = null;
                                        }
                                    }
                                    else {
                                        equipedItems[5] = inventory[currentY];
                                        inventory[currentY] = null;
                                    }
                                    break;
                            }
                        }
                        else {
                            for (int i = 0; i < inventory.length; i++) {
                                if (inventory[i] == null) {
                                    inventory[i] = equipedItems[currentY];
                                    equipedItems[currentY] = null;
                                }
                            }
                            if (equipedItems[currentY] != null) {
                                equipedItems[currentY].setX(player.getX());
                                equipedItems[currentY].setY(player.getY());
                                groundItems.add(equipedItems[currentY]);
                                equipedItems[currentY] = null;
                            }
                        }
                        break;
                    case 'w':
                        if (currentX == 0 && currentY == 0) {
                            currentY = inventory.length-1;
                        }
                        else if (currentX == 1 && currentY == 0) {
                            currentY = equipedItems.length-1;
                        }
                        else {
                            currentY--;
                        }
                        break;
                    case 'a':
                        if (currentX == 0) {
                            if (currentY > equipedItems.length) {
                                currentY = equipedItems.length-1;
                            }
                            currentX = 1;
                        }
                        else {
                            currentX = 0;
                        }
                        break;
                    case 's':
                        if (currentX == 0 && currentY == inventory.length-1) {
                            currentY = 0;
                        }
                        else if (currentX == 1 && currentY == equipedItems.length-1) {
                            currentY = 0;
                        }
                        else {
                            currentY++;
                        }
                        break;
                    case 'd':
                        if (currentX == 0) {
                            if (currentY > equipedItems.length) {
                                currentY = equipedItems.length-1;
                            }
                            currentX = 1;
                        }
                        else {
                            currentX = 0;
                        }
                        break;
                }
            }
        }
        terminal.clearScreen();
        return new InventoryUpdate(inventory, equipedItems, groundItems);
    }

}
