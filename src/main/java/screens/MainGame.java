package screens;

import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.terminal.Terminal;
import entities.Bat;
import entities.Creature;
import entities.Player;
import items.InventoryUpdate;
import items.Item;
import items.weapon.CopperShortSword;
import main.PrintWorld;
import world.Room;
import world.Rooms.startRoom;
import world.WorldTile;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;

public class MainGame {
    private Room tetingRoom = new startRoom(0, 0);
    private WorldTile[][] world = tetingRoom.getWorld();


    public boolean playerMoveCheck(int x, int y, LinkedList<Creature> creatures, WorldTile[][] world) {
        for (int i = 0; i < creatures.size(); i++) {
            if (creatures.get(i).getX() == x && creatures.get(i).getY() == y) {
                creatures.get(i).onBump();
                return false;
            }
        }
        if (!world[x][y].isWalkable()) {
            world[x][y].onBump();
            return false;
        }
        return true;
    }
    public static boolean creatureMoveCheck(int x, int y, LinkedList<Creature> creatures, WorldTile[][] world) {
        for (int i = 0; i < creatures.size(); i++) {
            if (creatures.get(i).getX() == x && creatures.get(i).getY() == y) {
                return false;
            }
        }
        if (!world[x][y].isWalkable()) {
            return false;
        }
        return true;
    }



    public void startGame(Terminal terminal, TextGraphics textGraphics) throws IOException, InterruptedException {
        /*
        generate world
        MAINLOOP:
        draw world
        add player, mosters, items
        add them all to the associated lists
        draw player, monsters, and items
        flush screen
        wait for user input
        prosess input
        do monster ai
        goto MAINLOOP
         */





        main.GameWorld.drawWorld(terminal, textGraphics, world); //prrint starter room
        //set up creatures and player
        LinkedList<Creature> creatures = new LinkedList<Creature>();
        Player player = new Player(2, 2);
        creatures.add(player);
        creatures.add(new Bat(5, 10));
        //set up items
        LinkedList<Item> items = new LinkedList<Item>();
        items.add(new CopperShortSword(5, 3));
        Item[] inventory = new Item[20];


        for (int i = 0; i < creatures.size(); i++) {
            textGraphics.setForegroundColor(creatures.get(i).getColor());
            textGraphics.setCharacter(creatures.get(i).getX(), creatures.get(i).getY(), creatures.get(i).getDisplayChar());
            System.out.println("printing " + creatures.get(i).getName());
        }
        terminal.flush();
        textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);


        while (true) {
            KeyStroke input = terminal.readInput();
            if (input.getKeyType() == KeyType.Escape) {
                break;
            }
            if (input.getCharacter() != null) {
                switch (input.getCharacter()) {
                    case 'w':
                        if (playerMoveCheck(player.getX(), player.getY() - 1, creatures, world)) {
                            player.setY(player.getY() - 1);
                        }
                        break;
                    case 'a':
                        if (playerMoveCheck(player.getX() - 1, player.getY(), creatures, world)) {
                            player.setX(player.getX() - 1);
                        }
                        break;
                    case 's':
                        if (playerMoveCheck(player.getX(), player.getY() + 1, creatures, world)) {
                            player.setY(player.getY() + 1);
                        }
                        break;
                    case 'd':
                        if (playerMoveCheck(player.getX() + 1, player.getY(), creatures, world)) {
                            player.setX(player.getX() + 1);
                        }
                        break;
                    case 'e':
                        for (int i = 0; i < items.size(); i++) {
                            if (items.get(i).getX() == player.getX() && items.get(i).getY() == player.getY()) {
                                if (new ItemPickupPopup().displayItemPopup(inventory, items.get(i), terminal, textGraphics)) {
                                    for (int j = 0; j < inventory.length; j++) {
                                        if (!(inventory[j] instanceof Item)) {
                                            inventory[j] = items.get(i);
                                            items.remove(i);
                                            terminal.clearScreen();
                                            new PrintWorld(creatures, items, textGraphics, terminal, world);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case 'i':
                        InventoryUpdate hack = new Inventory().displayInventory(inventory, player.getEquippedItems(), items, terminal, textGraphics, player);
                        items = hack.getGroundItems();
                        player.setEquippedItems(hack.getEquippedItems());
                        inventory = hack.getInventory();
                        break;
                }
            /*
            enemys move you are doing this ineffecently but thats ok for now i think
             */
                for (int i = 0; i < creatures.size(); i++) {
                    if (!(creatures.get(i) instanceof Player)) {
                        creatures.get(i).move(creatures, player, world);
                    }
                }
            }

            new main.PrintWorld(creatures, items, textGraphics, terminal, world);
        }
    }
}
