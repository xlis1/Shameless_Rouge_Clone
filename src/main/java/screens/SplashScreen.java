package screens;

import com.googlecode.lanterna.SGR;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalResizeListener;

import java.io.IOException;

public class SplashScreen {
    public void displaySplash(Terminal terminal, TextGraphics textGraphics) throws IOException {
        textGraphics.setForegroundColor(TextColor.ANSI.WHITE);
        textGraphics.setBackgroundColor(TextColor.ANSI.BLACK);
        textGraphics.putString(2, 1, "Welcome to SRC - Press Enter to Continue", SGR.BOLD);
        textGraphics.putString(2, 2, "(shameless rouge clone)");
        textGraphics.putString(2, 5, "It is strongly reccomended to make this window larger");
        textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);
        textGraphics.setBackgroundColor(TextColor.ANSI.DEFAULT);
        terminal.flush();

        terminal.addResizeListener(new TerminalResizeListener() {
            @Override
            public void onResized(Terminal terminal, TerminalSize newSize) {
                try {
                    terminal.flush();
                } catch (IOException e) {
                    // Not much we can do here
                    throw new RuntimeException(e);
                }
            }
        });
        while (true) {
            KeyStroke keyStroke = terminal.pollInput();
            if (keyStroke != null && (keyStroke.getKeyType() == KeyType.Enter || keyStroke.getKeyType() == KeyType.EOF)) {
                break;
            }
        }
        terminal.bell();
    }
}
