package screens;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.terminal.Terminal;
import items.Item;

import java.io.IOException;

public class ItemPickupPopup {
    public boolean displayItemPopup (Item[] inventory, Item item, Terminal terminal, TextGraphics textGraphics) throws IOException {
        int foo = 0;
        for (Item anInventory : inventory) {
            if (anInventory == null) {
                foo++;
            }
        }
        if (foo <= 0) {
            System.out.println("player attempted to grab item with full inventory");
            return false;
        }
        else {
            TerminalPosition pos = new TerminalPosition(terminal.getTerminalSize().getRows()-6, 0);
            TerminalSize size = new TerminalSize(("  Would you like to pick up " + item.getName() + " ?  ").length() , 6);
            textGraphics.fillRectangle(pos, size, ' ');
            textGraphics.drawLine(pos.getColumn(), 0, pos.getColumn() + ("  Would you like to pick up " + item.getName() + "?  ").length(), 0, '═');
            textGraphics.drawLine(pos.getColumn(), 6, pos.getColumn() + ("  Would you like to pick up " + item.getName() + "?  ").length(), 6, '═');
            textGraphics.drawLine(pos.getColumn(), 0, pos.getColumn(), 6, '║');
            textGraphics.drawLine(pos.getColumn()  + ("  Would you like to pick up " + item.getName() + " ?  ").length(), 0, pos.getColumn()  + ("  Would you like to pick up " + item.getName() + " ?  ").length(), 6, '║');
            textGraphics.setCharacter(pos, '╔');
            textGraphics.setCharacter(pos.getColumn(), pos.getRow() + 6, '╚');
            textGraphics.setCharacter(pos.getColumn() + ("  Would you like to pick up " + item.getName() + " ?  ").length(), pos.getRow() + 6, '╝');
            textGraphics.setCharacter(pos.getColumn() + ("  Would you like to pick up " + item.getName() + " ?  ").length(), pos.getRow(), '╗');
            textGraphics.putString(pos.getColumn() + 2, pos.getRow() + 2, "Would you like to pick up ");
            textGraphics.setForegroundColor(item.getTextColor());
            textGraphics.putString(pos.getColumn() + 2 +("Would you like to pick up ".length()), pos.getRow() + 2,  item.getName());
            textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);
            textGraphics.putString(pos.getColumn() + 2 +(("Would you like to pick up " + item.getName()).length()), pos.getRow() + 2, "?");
            textGraphics.putString(pos.getColumn() + 4, 4, "Y/N");
            terminal.flush();
            boolean looping = true;
            while (looping) {
                KeyStroke input = terminal.readInput();
                if (input.getKeyType() == KeyType.Escape) {
                    break;
                }
                if (input.getCharacter() != null) {
                    switch (input.getCharacter()) {
                        case 'y':
                            return true;
                        case 'n':
                            return false;
                    }
                }
            }
        }
        return false;
    }
}
