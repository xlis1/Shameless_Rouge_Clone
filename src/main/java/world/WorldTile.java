package world;

import com.googlecode.lanterna.TextColor;

public interface WorldTile {
    void onBump();
    char getChar();
    TextColor getColor();
    TextColor getBackColor();
    boolean isWalkable();
    String getName();
    String getDescription();
}
