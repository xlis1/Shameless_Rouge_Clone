package world.tiles;

import com.googlecode.lanterna.TextColor;
import world.WorldTile;

public class FloorTile implements WorldTile {

    public void onBump() {}

    public char getChar() {
        return '░';
    }

    public TextColor getColor() {
        return TextColor.ANSI.WHITE;
    }

    public TextColor getBackColor() {
        return TextColor.ANSI.BLACK;
    }

    public boolean isWalkable() {
        return true;
    }

    public String getName() {
        return "Floor";
    }

    public String getDescription() {
        return "A empty section of floor";
    }
}
