package world.tiles;

import com.googlecode.lanterna.TextColor;
import world.WorldTile;

public class WallTile implements WorldTile {

    public void onBump() {
        System.out.println("Player bumped into a wall");
    }

    public char getChar() {
        return '█';
    }

    public TextColor getColor() {
        return null;
    }

    public TextColor getBackColor() {
        return null;
    }

    public boolean isWalkable() {
        return false;
    }

    public String getName() {
        return "Wall";
    }

    public String getDescription() {
        return "A solid, unpassable, section of wall.";
    }
}
