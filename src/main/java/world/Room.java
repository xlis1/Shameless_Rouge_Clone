package world;

import entities.Creature;
import items.Item;

import java.util.LinkedList;

public class Room {
    private boolean hasNorthExit;
    private boolean hasEastExit;
    private boolean hasWestExit;
    private boolean hasSouthExit;
    private LinkedList<Creature> creatures;
    private LinkedList<Item> items;
    private WorldTile[][] world;
    private int gridX;
    private int gridY;

    public LinkedList<Creature> getCreatures() {
        return creatures;
    }

    public void setCreatures(LinkedList<Creature> creatures) {
        this.creatures = creatures;
    }

    public LinkedList<Item> getItems() {
        return items;
    }

    public void setItems(LinkedList<Item> items) {
        this.items = items;
    }

    public int getGridX() {
        return gridX;
    }

    public void setGridX(int gridX) {
        this.gridX = gridX;
    }

    public int getGridY() {
        return gridY;
    }

    public void setGridY(int gridY) {
        this.gridY = gridY;
    }

    public boolean isHasNorthExit() {
        return hasNorthExit;
    }

    public void setHasNorthExit(boolean hasNorthExit) {
        this.hasNorthExit = hasNorthExit;
    }

    public boolean isHasEastExit() {
        return hasEastExit;
    }

    public void setHasEastExit(boolean hasEastExit) {
        this.hasEastExit = hasEastExit;
    }

    public boolean isHasWestExit() {
        return hasWestExit;
    }

    public void setHasWestExit(boolean hasWestExit) {
        this.hasWestExit = hasWestExit;
    }

    public WorldTile[][] getWorld() {
        return world;
    }

    public void setWorld(WorldTile[][] world) {
        this.world = world;
    }

    public boolean isHasSouthExit() {
        return hasSouthExit;
    }

    public void setHasSouthExit(boolean hasSouthExit) {
        this.hasSouthExit = hasSouthExit;
    }
}
