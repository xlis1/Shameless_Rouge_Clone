package world.Rooms;

import main.GameWorld;
import world.Room;
import world.WorldTile;
import world.tiles.FloorTile;

public class NECornerRoom extends Room {
    public NECornerRoom(int x, int y) {
        this.setGridX(x);
        this.setGridY(y);
        this.setHasEastExit(true);
        this.setHasNorthExit(true);
        this.setHasSouthExit(false);
        this.setHasWestExit(false);
        WorldTile[][] world = GameWorld.generateWorld();
        world[world.length-1][world[0].length/2] = new FloorTile();
        world[world.length/2][world[0].length-1] = new FloorTile();
        world[world.length/2][0] = new FloorTile();
        world[0][world[0].length/2] = new FloorTile();

        this.setWorld(world);
    }
}
