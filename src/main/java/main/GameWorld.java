package main;

import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.terminal.Terminal;
import world.WorldTile;
import world.tiles.FloorTile;
import world.tiles.WallTile;
import java.io.IOException;

public class GameWorld {
    private static WorldTile[][] world;

    public static WorldTile[][] generateWorld() {
        world = new WorldTile[25][15];

        for (int i = 0; i < world.length; i++) {
            for (int j = 0; j < world[i].length; j++) {
                if (i == 0 || j == 0 || i == world.length-1 || j == world[i].length-1) {
                    world[i][j] = new WallTile();
                }
                else {
                    world[i][j] = new FloorTile();
                }
            }
        }
        return world;
}
    public WorldTile[][] getWorld() {
        return world;
    }
    public void setWorld(WorldTile[][] world) {
        this.world = world;
    }
    public static void drawWorld(Terminal terminal, TextGraphics textGraphics, WorldTile[][] world) throws IOException {
        terminal.clearScreen();
        for (int i = 0; i < world.length; i++) {
            for (int j = 0; j < world[i].length; j++) {
                textGraphics.setCharacter(i, j, world[i][j].getChar());
            }
        }
        terminal.flush();
        System.out.println("world painted");
    }
}
