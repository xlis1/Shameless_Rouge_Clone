package main;

import com.googlecode.lanterna.graphics.*;
import com.googlecode.lanterna.terminal.*;
import com.sun.javafx.applet.Splash;
import screens.MainGame;
import screens.SplashScreen;

import java.io.IOException;

public class RougeClone {

    public static void main(String args[]) {
        DefaultTerminalFactory defaultTerminalFactory = new DefaultTerminalFactory();
        Terminal terminal = null;

        try {
            terminal = defaultTerminalFactory.createTerminal();
            terminal.enterPrivateMode();
            terminal.clearScreen();
            terminal.setCursorVisible(false);
            final TextGraphics textGraphics = terminal.newTextGraphics();

            new SplashScreen().displaySplash(terminal, textGraphics);


            //TODO: actually make the fucking game
            //im just gonna write out the structure i think im gonna use and hopefully that will help me do it
            //all game functions moved to their own classes in screens package for readability and tidyness
            new MainGame().startGame(terminal, textGraphics);


        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (terminal != null) {
                try {
                    /*
                    The close() call here will exit private mode
                     */
                    terminal.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}