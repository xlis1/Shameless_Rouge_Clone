package main;

import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.terminal.Terminal;
import entities.Creature;
import items.Item;
import world.WorldTile;

import java.io.IOException;
import java.util.LinkedList;

public class PrintWorld {

    public PrintWorld(LinkedList<Creature> creatures, LinkedList<Item> items, TextGraphics textGraphics, Terminal terminal, WorldTile[][] world) throws IOException {

        for (int i = 0; i < world.length; i++) {
            for (int j = 0; j < world[i].length; j++) {
                textGraphics.setCharacter(i, j, world[i][j].getChar());
            }
        }

        for (Item item : items) {
            textGraphics.setForegroundColor(item.getTextColor());
            textGraphics.setCharacter(item.getX(), item.getY(), item.getDisplayChar());
        }
        textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);


        for (Creature creature : creatures) {
            textGraphics.setForegroundColor(creature.getColor());
            textGraphics.setCharacter(creature.getX(), creature.getY(), creature.getDisplayChar());
        }
        textGraphics.setForegroundColor(TextColor.ANSI.DEFAULT);
        terminal.flush();
    }
}
