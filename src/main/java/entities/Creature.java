package entities;

import com.googlecode.lanterna.TextColor;
import screens.MainGame;
import world.WorldTile;

import java.util.LinkedList;
import java.util.Random;

public class Creature {
    private int health;
    private int maxHealth;
    private char displayChar;
    private String name;
    private int x;
    private int y;
    private TextColor color;

    public TextColor getColor() {
        return color;
    }

    public void setColor(TextColor color) {
        this.color = color;
    }

    public void onBump() {}
    public void onDestroy() {}
    public void attack(Creature target) {}
    public void move(LinkedList<Creature> creatures, Player player, WorldTile[][] world) {
        Random rand = new Random();
        if (rand.nextInt(5) > 1) {
            for (int i = 0; i < creatures.size(); i++) {
                if (!(creatures.get(i) instanceof Player)) {
                    if (creatures.get(i).getX() - player.getX() <= 1 && creatures.get(i).getX() - player.getX() >= -1 && creatures.get(i).getY() - player.getY() <= 1 && creatures.get(i).getY() - player.getY() >= -1) {
                        creatures.get(i).attack(player);
                        System.out.println(creatures.get(i).getName() + " attacks the player");
                    } else if (creatures.get(i).getY() > player.getY() && MainGame.creatureMoveCheck(creatures.get(i).getX(), creatures.get(i).getY() - 1, creatures, world)) {
                        creatures.get(i).setY(creatures.get(i).getY() - 1);
                    } else if (creatures.get(i).getY() < player.getY() && MainGame.creatureMoveCheck(creatures.get(i).getX(), creatures.get(i).getY() + 1, creatures, world)) {
                        creatures.get(i).setY(creatures.get(i).getY() + 1);
                    } else if (creatures.get(i).getX() < player.getX() && MainGame.creatureMoveCheck(creatures.get(i).getX() + 1, creatures.get(i).getY(), creatures, world)) {
                        creatures.get(i).setX(creatures.get(i).getX() + 1);
                    } else if (creatures.get(i).getX() > player.getX() && MainGame.creatureMoveCheck(creatures.get(i).getX() - 1, creatures.get(i).getY(), creatures, world)) {
                        creatures.get(i).setX(creatures.get(i).getX() - 1);
                    }
                }
            }
        }
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxhealth) {
        this.maxHealth = maxhealth;
    }

    public char getDisplayChar() {
        return displayChar;
    }

    public void setDisplayChar(char displayChar) {
        this.displayChar = displayChar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
