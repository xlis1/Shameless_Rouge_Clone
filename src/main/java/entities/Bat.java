package entities;

import com.googlecode.lanterna.TextColor;

public class Bat extends Creature {
    public Bat(int x, int y) {
        this.setX(x);
        this.setY(y);
        this.setName("Bat");
        this.setColor(TextColor.ANSI.RED);
        this.setDisplayChar('x');
        this.setHealth(20);
        this.setMaxHealth(20);
    }
}
