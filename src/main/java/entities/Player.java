package entities;

import com.googlecode.lanterna.TextColor;
import items.Item;

public class Player extends Creature {
    private Item[] equippedItems;
    // EQUIPPED ITEMS FORMAT: HEAD, CHEST, GLOVES, BOOTS, BACK, ONE_HAND, TWO_HANDS
    public Item[] getEquippedItems() {
        return equippedItems;
    }

    public void setEquippedItems(Item[] equippedItems) {
        this.equippedItems = equippedItems;
    }

    public Player(int x, int y) {
        this.setX(x);
        this.setY(y);
        this.setMaxHealth(100);
        this.setDisplayChar('@');
        this.setName("Player");
        this.setColor(TextColor.ANSI.GREEN);
        this.equippedItems = new Item[7];
    }
}
